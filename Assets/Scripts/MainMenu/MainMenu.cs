﻿
using UnityEngine;
using ImGuiNET;

public class MainMenu : MonoBehaviour
{
	void OnEnable()
	{
		ImGuiUn.Layout += OnLayout;
	}

	void OnDisable()
	{
		ImGuiUn.Layout -= OnLayout;
	}

	void OnLayout()
	{
		ImGui.ShowDemoWindow();
	}
}